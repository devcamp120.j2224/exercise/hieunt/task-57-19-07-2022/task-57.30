package com.task5730.absclass.model;

public class Duck extends Animal {
    private String beakColor; 
    public Duck(int age,
    String gender, String beakColor) {
        super(age, gender);
        this.beakColor = beakColor;
    }
    public String getBeakColor() {
        return beakColor;
    }
    public void setBeakColor(String beakColor) {
        this.beakColor = beakColor;
    }
    @Override
    public boolean isMammal() {
        return false;
    }
    @Override
    public void mate() {
        System.out.println("Duck mates");
    }
    public void swim() {
        System.out.println("Duck swims");
    }
    public void quack() {
        System.out.println("Duck quacks");
    }
}
