package com.task5730.absclass.model;

import java.util.ArrayList;

public class Worker extends Person {
	private int salary;
	// khởi tạo thiếu salary
	public Worker(int age, String gender,
	String name, Address address,
	ArrayList<Animal> listPet) {
		super(age, gender, name, address, listPet);
		this.salary = 5000;
	}
	// khởi tạo đầy đủ tham số
	public Worker(int age, String gender,
	String name, Address address,
	ArrayList<Animal> listPet, int salary) {
		super(age, gender, name, address, listPet);
		this.salary = salary;
	}
	@Override
	public void eat() {
		System.out.println("Worker is eating");
	}
	public void working() {
		System.out.println("Worker is working");
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
}
