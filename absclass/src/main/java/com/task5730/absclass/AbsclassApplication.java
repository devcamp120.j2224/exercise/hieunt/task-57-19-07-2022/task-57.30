package com.task5730.absclass;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AbsclassApplication {

	public static void main(String[] args) {
		SpringApplication.run(AbsclassApplication.class, args);
	}

}
