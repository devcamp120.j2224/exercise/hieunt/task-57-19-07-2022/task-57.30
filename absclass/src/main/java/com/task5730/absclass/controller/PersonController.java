package com.task5730.absclass.controller;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.task5730.absclass.model.*;

@RestController
public class PersonController {
	@CrossOrigin
	@GetMapping("/person")
	public ArrayList<Person> getPersonList(@RequestParam(defaultValue = "0") String type) {
		// Tạo danh sách Student
		ArrayList<Person> studentList = new ArrayList<Person>();		
		// Tạo danh sách pet 1
		ArrayList<Animal> petList1 = new ArrayList<Animal>();
        Animal pet1 = new Duck(2, "male", "yellow");
        Animal pet2 = new Fish(1, "female", 3, true);
        petList1.add(pet1);
        petList1.add(pet2);
		// Tạo danh sách pet 2
		ArrayList<Animal> petList2 = new ArrayList<Animal>();
        Animal pet3 = new Zebra(3, "male", true);
        Animal pet4 = new Duck(1, "female", "red");
        petList2.add(pet3);
        petList2.add(pet4);
		// Tạo danh sách Worker
		ArrayList<Person> workerList = new ArrayList<Person>();	
		Person worker1 = new Worker(24, "male", "Phong", new Address(), petList1);
		Person worker2 = new Worker(30, "female", "Trinh", new Address(), petList2, 9500);
		Person worker3 = new Worker(34, "male", "Sinh", new Address(), null, 7680);
		workerList.add(worker1);
		workerList.add(worker2);
		workerList.add(worker3);
		// Tạo danh sách Professor
		ArrayList<Person> professorList = new ArrayList<Person>();
		Person professor1 = new Professor(50, "male", "Minh", new Address(), petList1);	
		Person professor2 = new Professor(55, "female", "Huong", new Address(), null, 7000);
		Person professor3 = new Professor(45, "male", "Thang", new Address(), petList2);
		Person professor4 = new Professor(50, "female", "Thu", new Address(), null, 8000);
		Person professor5 = new Professor(55, "male", "Trung", new Address(), null);
		Person professor6 = new Professor(45, "female", "Trang", new Address(), null);
		professorList.add(professor1);
		professorList.add(professor2);
		professorList.add(professor3);
		professorList.add(professor4);
		professorList.add(professor5);
		professorList.add(professor6);
		// Tạo danh sách subject 1
		ArrayList<Subject> subjectList1 = new ArrayList<Subject>();
		Subject subject01 = new Subject("Toan", 1, (Professor) professor1);
		Subject subject02 = new Subject("Van", 2, (Professor) professor2);
		subjectList1.add(subject01);
		subjectList1.add(subject02);
		// Tạo danh sách subject 2
		ArrayList<Subject> subjectList2 = new ArrayList<Subject>() {
			{
				add(new Subject("Vat Ly", 3, (Professor) professor3));
				add(new Subject("Hoa Hoc", 4, (Professor) professor4));
			}
		};
		// Khởi tạo các đối tượng thuộc lớp Student
		Person student1 = new Student(18, "male", "Nam", new Address(), petList1, 1, subjectList1);
		Person student2 = new Student(19, "female", "Hoa", new Address(), petList2, 2, subjectList2);
		Person student3 = new Student(20, "female", "Lan", new Address(), null, 3,
			new ArrayList<Subject>(Arrays.asList(
				new Subject("Sinh Hoc", 5, (Professor) professor5),
				new Subject("Lich Su", 6, (Professor) professor6)
			))
		);
		// thêm các student và studentList	
		studentList.add(student1);
		studentList.add(student2);
		studentList.add(student3);
		// tạo danh sách Person gồm các student, worker, professor
		ArrayList<Person> personList = new ArrayList<Person>();
		for (Person student : studentList) {
			personList.add(student);
		}
		for (Person worker : workerList) {
			personList.add(worker);
		}
		for (Person professor : professorList) {
			personList.add(professor);
		}
		ArrayList<Person> result = null;
		if(type.equals("1")) {
			result = studentList;
		}
		else if(type.equals("2")) {
			result = workerList;
		}
		else if(type.equals("3")) {
			result = professorList;
		}
		else {
			result = personList;
		}
		return result;
	}
}
